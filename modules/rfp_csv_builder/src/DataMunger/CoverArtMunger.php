<?php

namespace Drupal\rfp_csv_builder\DataMunger;

use Drupal\file\Entity\File;

/**
 * Cover art munger.
 */
class CoverArtMunger {

  /**
   * Munge the covers data into a format we like.
   */
  public function munge($conn) {

    \Drupal::logger('rfp_csv_builder')->notice('Creating file entities for cover art');
    \Drupal::logger('rfp_csv_builder')->error('TODO - check to see if we have already created the files!');
    // Grab the collections csv.
    $fh = fopen(DRUPAL_ROOT . '/' .
          drupal_get_path('module', 'rfp_csv_builder') . '/csv/collections.csv', 'r') or die('could not open collections csv for munging!');

    // Look up the file entities in the new db.
    while ($data = fgetcsv($fh)) {

      $imguri = $data[2];
      if ($imguri == 'cover_art') {
        continue;
      }

      if (!file_exists($imguri)) {
        $filename = basename($imguri);
        $src = 'http://lcmp.trentradio.ca:17080/sites/lcmp.trentradio.ca/files/' . $filename;
        file_put_contents('/canonical_local/collection_covers/' . $filename,
          file_get_contents($src));
      }

      $filename = basename($imguri);
      // Create the file.
      $file = File::create([
        'uid' => 1,
        'filename' => $filename,
        'uri' => 'public://images/covers/' . $filename,
        'status' => 1,
      ]);
      $file->save();

      copy($imguri, '/canonical_local/radiofreenetwork/web/sites/default/files/images/covers/' . $filename);

    }

    // Make media / file entities
    // Associate them with the collection's cover art field
    // Log success / failure.
  }

}
