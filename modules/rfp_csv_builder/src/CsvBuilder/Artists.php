<?php

namespace Drupal\rfp_csv_builder\CsvBuilder;

use Drupal\Core\Database\Database;
use Drupal\Core\Logger\LoggerChannelTrait;

/**
 * Builder for RFP Artists csv.
 */
class Artists {

  use LoggerChannelTrait;

  /**
   * Build artists csv.
   */
  public function build() {

    $this->writeArtists($this->fetchArtists());
  }

  /**
   * Fetch artists from database.
   */
  public function fetchArtists() {

    $unique = [];

    // Switch to external database.
    Database::setActiveConnection('migrate');
    $db = Database::getConnection();
    $query = $db->query("SELECT title, nid FROM node WHERE type='artist' ORDER BY title");
    $artists = $query->fetchAll();
    foreach ($artists as $artist) {
      $name = utf8_decode($artist->title);
      $unique['artist_' . $artist->nid] = $name;
    }

    // Flip back to default db.
    Database::setActiveConnection();
    return $unique;
  }

  /**
   * Write artists.
   */
  public function writeArtists($artists) {

    $targetDir = drupal_get_path('module', 'rfp_csv_builder') . '/csv/';
    $targetFile = $targetDir . 'artists.csv';
    if (!file_exists($targetDir)) {
      mkdir($targetDir);
    }
    $fh = fopen($targetFile, 'w');

    fputcsv($fh, ['field_call_number', 'title']);

    foreach ($artists as $key => $fields) {
      fputcsv($fh, [$key, $fields]);
    }
    fclose($fh);
    $this->getLogger('rfp_csv')->notice('Wrote ' . count($artists) . ' artists to ' . $targetFile);

  }

}
