<?php

namespace Drupal\rfp_csv_builder\CsvBuilder;

use Drupal\Core\Database\Database;

/**
 * Csv tracks builder.
 */
class Tracks {

  /**
   * Build the tracks csv.
   */
  public function build() {

    $this->writeTracks($this->fetchTracks());
  }

  /**
   * Fetch the tracks from the db.
   */
  public function fetchTracks() {

    $trackData = [];

    // Switch to external database.
    Database::setActiveConnection('migrate');
    $db = Database::getConnection();
    $query = $db->query("SELECT node.nid as nid, node.title as title, field_data_field_mp3.field_mp3_value as mp3,
        field_data_field_artists.field_artists_target_id as artist_nid,
        field_data_field_track_number.field_track_number_value as track_number,
        field_data_field_duration.field_duration_value as duration,
        field_data_field_albums.field_albums_target_id as album_nid

        FROM  field_data_field_albums
        LEFT JOIN node ON node.nid = field_data_field_albums.entity_id
        LEFT JOIN field_data_field_mp3  ON field_data_field_mp3.entity_id = node.nid
        LEFT JOIN field_data_field_artists ON field_data_field_artists.entity_id = node.nid
        LEFT JOIN field_data_field_duration ON field_data_field_duration.entity_id = node.nid
        LEFT JOIN field_data_field_track_number ON field_data_field_track_number.entity_id = node.nid

        ORDER BY field_data_field_track_number.field_track_number_value");
    $tracks = $query->fetchAll();

    foreach ($tracks as $track) {

      $mp3 = str_replace('sites/lcmp.trentradio.ca/rfp_mp3/', '', $track->mp3);
      $tracknum = $track->track_number;
      if (!$tracknum) {
        $tracknum = 0;
      }

      $tracknum = 'track_' . $tracknum;

      $trackData[] = [
        'track_' . $track->nid,
        utf8_decode($track->title),
        $mp3,
        'artist_' . $track->artist_nid,
        'collection_' . $track->album_nid,
        $track->duration,
        $track->track_number,
      ];
    }

    // Flip back to default db.
    Database::setActiveConnection();
    return $tracks;
  }

  /**
   * Write tracks to csv.
   */
  public function writeTracks($tracks) {

    $targetDir = drupal_get_path('module', 'rfp_csv_builder') . '/csv/';
    $targetFile = $targetDir . 'tracks.csv';
    if (!file_exists($targetDir)) {
      mkdir($targetDir);
    }
    $fh = fopen($targetFile, 'w');
    $counter = 0;

    fputcsv($fh, [
      'field_call_number',
      'title',
      'mp3',
      'artist_call_number',
      'album_call_number',
      'duration',
      'track_number',
    ]);
    foreach ($tracks as $data) {

      $row = [
        'track_' . $data->nid,
        $data->title,
        $data->mp3,
        'artist_' . $data->artist_nid,
        'collection_' . $data->album_nid,
        $data->duration,
        $data->track_number,
      ];

      // Fix bad characters that shouldn't be in filenames.
      if (stristr($data->title, 'Mari?e')) {

        $row[1] = str_replace('Mari?e', 'Mariee', $data->title);
        $row[2] = preg_replace('/Mari.e/', 'Mariee', $data->mp3);
      }

      // Strip out old site path.
      $row[2] = str_replace('sites/lcmp.trentradio.ca/rfp_mp3/', '', $row[2]);

      fputcsv($fh, $row);
      ++$counter;
    }
    fclose($fh);
    \Drupal::logger('tracks')->notice('Wrote ' . $counter . ' tracks to ' . $targetFile);
  }

}
