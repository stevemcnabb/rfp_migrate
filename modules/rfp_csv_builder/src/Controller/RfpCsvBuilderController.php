<?php

namespace Drupal\rfp_csv_builder\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for RFP CSV Builder routes.
 */
class RfpCsvBuilderController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
